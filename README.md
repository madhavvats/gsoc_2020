# Selection Test

Files added:-modules_parser.py,sample.json

To generate the JSON file, run the command:-"python3 modules_parser.py".

Information about the module files extracted and added in JSON file:-{name_of_the_module,current_version(specified in .version file),all the versions available,path of the current version}

Relevant info that I think should also be included in the software catalog:-

1.Link to module's website/repository

2.Info about the module(purpose/usage of the module)

3.Date/Time it was added, or its version was changed

The following are the ways of which I can think to extract the above info:-

Manually add the above info for the currently uploaded modules and add sections for the following in the file:-module_install_template.sh so that in future whenever a new module is added to Genpipes we would have the following info readily available and the catalog would be updated with the following info automatically.


## Automatic update of the C3G software stack catalog

GenPipes is one of the main bioinformatics tools/services offered by the [Canadian Center for Computational genomics (C3G)](http://www.computationalgenomics.ca). It consists of bioinformatics pipelines designed for Genomics sequencing analysis.
In order to perform those analysis, the C3G developers maintain a software stack which continuously grows and updates.
A catalog of the installed softwares was built in JSON format and is available on the C3G website (http://www.computationalgenomics.ca/cvmfs-modules/).

This project consists of implementing a system to automate the update of the JSON catalog as new versions and softwares are installed. This system will have to be fully integrated in the current installation workflow of the C3G developers (https://bitbucket.org/mugqic/genpipes/src/master/#markdown-header-install-a-new-module) and eventually be able to parse Tcl module files (https://modules.readthedocs.io/en/stable/modulefile.html) which would represent the source of the catalog.

### Developer profile

**Required skills:** JSON, Git, Python, Bash  
**Nice to have:** Tcl modules experience, software versioning

### Selection tests

The content of the `data_test/SoftStackCatalog` folder mimics the file structure of the C3G software stack, where the `modulefiles/mugqic` directory contains all the Tcl module files used by the GenPipes anaylsis pilelines to call all their required software.
The `software` directory would actually contain all the tools provided by the software stack.

Here the `modulefiles/mugqic` folder only contains a handful of the C3G module files. These modules refer to softwares installed in the `software` directory.

The `software` folder contains all the directory structures of the software specified in the `modulefiles/mugqic` module files (it does not contain the software itself as it is not necessary for the test).


1- Parse the whole file structure of the [`data_test/SoftStackCatalog/modulefiles/mugqic/`](https://bitbucket.org/mugqic/gsoc_2020/src/master/data_test/SoftStackCatalog/modulefiles/mugqic/) folder and create a JSON file that would represent all the provided software and available versions.

2- Identify the lacking information for a comprehensive and meaningful software catalog : besides the name and version of the software, what relevant information should be provided by the catalog (software description, website, etc...).
   Propose ways to find, gather and store this missing information.

Optionally, you could have a look at how we install software on the C3G software stack by looking at some of our installaton scripts [here](https://bitbucket.org/mugqic/genpipes/src/master/resources/modules/).
These scripts allow us to install software, as well as create the associated module files for the desired versions.

Save your work (script and result JSON) in an external repository and send us a link to it.
Additional information that you think of can be either sent by email or included in the repo within a text file.


### Mentors
[Edouard Henrion](mailto:edouard.henrion@computationalgenomics.ca)   
[Pierre-Olivier Quirion](mailto:po.quirion@mcgill.ca)  

-----------------------------

