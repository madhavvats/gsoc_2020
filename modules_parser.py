import re 
import os
import json
path=os.getcwd()+"/data_test/SoftStackCatalog/modulefiles/mugqic"
modules={}
#Loop through all module files
for modulename in os.listdir(path):
    print(modulename)
    module_info={}
    #Get current version from .version file
    with open(os.path.join(path, modulename+"/.version"), 'r') as f: 
        for content in f.readlines():
            if (re.search("set ModulesVersion",content)):
                module_info["Current version"]=content.split()[-1][1:-1]
    versions_available=[]
    #List all the versions available
    for ver in os.listdir(os.path.join(path, modulename)):
        if ver==".version":
            continue
        versions_available.append(ver)
    module_info["Versions available"]=versions_available
    #Get path of current version
    with open(os.path.join(path, modulename,module_info["Current version"]), 'r') as f:
        for content in f.readlines():
            if (re.search("set",content) and re.search("root",content)):
                module_info["Path to current version"]=content.split()[-1]
            if (re.search("#%Module",content)):
                module_info['Magic Number']=content[:-1]
    modules[modulename]=module_info
#Create json object
json_file=json.dumps(modules,indent=len(modules))
with open("sample.json", "w") as outfile: 
    outfile.write(json_file) 
